<?php defined('BASEPATH') or exit ('No direct script access allowed');


$config = array(

        'food' => array(

            array(
                'field'=>'name',
                'label'=>'name',
                'rules' => 'required|xss_clean|trim',
                ),
              array(
                'field'=>'phone',
                'label'=>'phone',
                'rules' => 'required|max_length[15]|min_length[9]|xss_clean|trim',
                ),
                array(
                'field'=>'email',
                'label'=>'email',
                'rules' => 'required|valid_email|xss_clean|trim',
                ),
                array(
                'field'=>'order',
                'label'=>'order',
                'rules' => 'required',
                ),

            )
);