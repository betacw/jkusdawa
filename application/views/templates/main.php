<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>JKUSDAWA</title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link rel="icon" type="image/jpg" href="<?php echo base_url('assets/images/jkusdawa.jpg');?>">

        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" >
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>" >
        <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.min.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/templatemo-style.css');?>">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    </head>
    <!-- 
        Crystal Template 
        http://www.templatemo.com/preview/templatemo_437_crystal
    -->
    <body data-spy="scroll" data-target=".navbar-collapse">

        <!-- start navigation -->

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="navbar-header">
                        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon icon-bar"></span>
                            <span class="icon icon-bar"></span>
                            <span class="icon icon-bar"></span>
                        </button>
                         
                    </div>
                    <div class="collapse navbar-collapse">

                    <a href="#" class="navbar-brand"><h3>JKUSDAWA</h3></a>

                        <ul class="nav navbar-nav navbar-right">

                            <li><a href="#home" class="smoothScroll">HOME</a></li>
                            <li><a href="#about" class="smoothScroll">ABOUT</a></li>
                            <li><a href="#team" class="smoothScroll">TEAM</a></li>
                            <li><a href="#service" class="smoothScroll">SERVICES</a></li>
                            <li><a href="#work" class="smoothScroll">WORK</a></li>
                            <li><a href="#pricing" class="smoothScroll">PRICING</a></li>
                            <li><a href="#contact" class="smoothScroll">CONTACT</a></li>
                             <li><a href="<?php echo site_url('superclass/catering');?>">CATERERS</a></li>

                              <span><img src="<?php echo base_url('assets/images/banner.jpg'); ?>" width="100px" height="70px"></span>
                        </ul>
                    </div>
                </div>              
            </div>
        </nav>
        <!-- end navigation -->

        <!-- start home -->
        <section id="home" class="text-center">
          <div class="templatemo_headerimage">
            <div class="flexslider">
              <ul class="slides">
                <li>
                    <img src="<?php echo base_url('assets/images/slider/1.jpg');?>" alt="Slide 1">
                    <div class="slider-caption">
                        <div class="templatemo_homewrapper">
                          <h1 class="wow fadeInDown" data-wow-delay="2000">WELCOME TO JKUSDAWA</h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
                            <span class="rotate">Imbued by the intergrity of Christ</span>
                        </h2>
                        <p>We provide services effieciently</p>
                        <a href="#work" class="smoothScroll btn btn-default wow fadeInDown" data-wow-delay="2000">Our Work</a>  
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?php echo base_url('assets/images/slider/2.jpg');?>" alt="Slide 2">
                    <div class="slider-caption">
                        <div class="templatemo_homewrapper">
                          <h1 class="wow fadeInDown" data-wow-delay="2000">Range of services</h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
                            <span class="rotate">Business made better</span>
                        </h2>
                        <p>Partner with us today</p>
                        <a href="#about" class="smoothScroll btn btn-default wow fadeInDown" data-wow-delay="2000">See about us</a> 
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?php echo base_url('assets/images/slider/3.jpg');?>" alt="Slide 3">
                    <div class="slider-caption">
                        <div class="templatemo_homewrapper">
                          <h1 class="wow fadeInDown" data-wow-delay="2000">Good food</h1>
                          <h2 class="wow fadeInDown" data-wow-delay="2000">
                            <span class="rotate">JKUSDAWA caterers provide good food</span>
                        </h2>
                        <p>Vegeterian meals that will help you keep fit and remain healthy</p>
                        <a href="#service" class="smoothScroll btn btn-default wow fadeInDown" data-wow-delay="2000">Our Services</a>   
                        </div>
                    </div>
                </li>
              </ul>
            </div>
          </div>                
        </section>
        <!-- end home -->
        <!-- start about -->
        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-7 wow fadeInLeft" data-wow-delay="2000">
                        <h3>Welcome to JKUSDA WELFARE SOCIETY</h3>
                        <h2>Always climbing With you</h2>
                        <p>We are a society under the JKUSDA church and we are priviledged to provide you with services on various fields that we will mention. This is all in a bid to provide a leaning shoulder for needy students.  <span class="blue">And</span><span class="green">   Also </span><span class="orange">Income for ministry and other activities that will ensure that the church is sustained.</span></p>
                    </div>
                    <div class="col-sm-6 col-md-5 wow fadeInRight" data-wow-delay="2000">
                        <h3>Our portfolio</h3>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <img src="<?php echo base_url('assets/images/company-img-1.jpg');?>" class="img-responsive" alt="company 1">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <img src="<?php echo base_url('assets/images/company-img-2.jpg');?>" class="img-responsive" alt="company 2">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <img src="<?php echo base_url('assets/images/company-img-3.jpg');?>" class="img-responsive" alt="company 3">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <img src="<?php echo base_url('assets/images/company-img-4.jpg');?>" class="img-responsive" alt="company 4">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end about -->
        <!-- start team -->
       <!-- <section id="team" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="900">
                        <h3>MEET JKUSDAWA TEAM</h3>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <p>We are divided into departments that specialize on different services. Some of which are listed below</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-sm-6 col-md-3 wow fadeInLet" data-wow-delay="2000">
                        <div class="team_thumb">
                            <img src="<?php echo base_url('assets/images/team-img-1.jpg');?>" class="img-responsive" alt="team">
                                <div class="team_overlay">
                                    <ul class="social_icon">
                                        <li><a href="#" class="fa fa-facebook"></a></li>
                                        <li><a href="#" class="fa fa-twitter"></a></li>
                                        <li><a href="#" class="fa fa-pinterest"></a></li>
                                    </ul>                               
                                </div>
                        </div>
                        <div class="team_description">
                            <h4>Catering</h4>
                            <h5><a href='#'>See More</a></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="2000">
                        <div class="team_thumb">
                            <img src="<?php echo base_url('assets/images/team-img-2.jpg');?>" class="img-responsive" alt="team">
                                <div class="team_overlay">
                                    <ul class="social_icon">
                                        <li><a href="#" class="fa fa-facebook"></a></li>
                                        <li><a href="#" class="fa fa-twitter"></a></li>
                                        <li><a href="#" class="fa fa-pinterest"></a></li>
                                    </ul>
                                </div>
                        </div>
                        <div class="team_description">
                            <h4>Hardware Development</h4>
                            <h5>Includes fabrication of electronic equipment</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="2000">
                        <div class="team_thumb">
                            <img src="<?php echo base_url('assets/images/team-img-3.jpg');?>" class="img-responsive" alt="team">
                                <div class="team_overlay">
                                    <ul class="social_icon">
                                        <li><a href="#" class="fa fa-facebook"></a></li>
                                        <li><a href="#" class="fa fa-twitter"></a></li>
                                        <li><a href="#" class="fa fa-pinterest"></a></li>
                                    </ul>
                                </div>
                        </div>
                        <div class="team_description">
                            <h4>Software Development</h4>
                            <h5>Includes web development, apps </h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 wow fadeInRight" data-wow-delay="2000">
                        <div class="team_thumb">
                            <img src="<?php echo base_url('assets/images/team-img-4.jpg');?>" class="img-responsive" alt="team">
                                <div class="team_overlay">
                                    <ul class="social_icon">
                                        <li><a href="#" class="fa fa-facebook"></a></li>
                                        <li><a href="#" class="fa fa-twitter"></a></li>
                                        <li><a href="#" class="fa fa-pinterest"></a></li>
                                    </ul>
                                </div>
                        </div>
                        <div class="team_description">
                            <h4>Software Design</h4>
                            <h5>Logo, branding </h5>
                        </div>
                    </div>
                            
                </div>
            </div>
        </section>
        <!-- end team -->
        -->
        <!-- start service -->
        <section id="service">
            <div class="container">
                <div class="row">
                    <div class="text-center col-md-12 wow fadeInDown" data-wow-delay="2000">
                        <h3>JKUSDAWA SERVICES</h3>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8 text-center">
                        <p>Mauris mattis vitae libero eget iaculis. Donec et augue quis quam porttitor consectetur ut nec sem. Integer sagittis viverra quam quis ultricies. Aenean risus nisl, consequat at nunc id, tincidunt tristique mauris.</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="2000">
                        <div class="media">
                            <i class="fa fa-cog pull-left media-object"></i>
                                <div class="media-body">
                                    <h4 class="media-heading">WEB DEVELOPMENT</h4>
                                    <p>In suscipit, purus id ultrices sodales, nunc mi porta nibh, bibendum maximus sem mi id metus. Suspendisse faucibus suscipit diam.</p>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInDown" data-wow-delay="2000">
                        <div class="media">
                            <i class="fa fa-globe pull-left media-object"></i>
                                <div class="media-body">
                                    <h4 class="media-heading">DOMAIN HOSTING</h4>
                                    <p>In suscipit, purus id ultrices sodales, nunc mi porta nibh, bibendum maximus sem mi id metus. Suspendisse faucibus suscipit diam.</p>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInRight" data-wow-delay="2000">
                        <div class="media">
                            <i class="fa fa-desktop pull-left media-object"></i>
                                <div class="media-body">
                                    <h4 class="media-heading">WEB DESIGN</h4>
                                    <p>In suscipit, purus id ultrices sodales, nunc mi porta nibh, bibendum maximus sem mi id metus. Suspendisse faucibus suscipit diam.</p>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="2000">
                        <div class="media">
                            <i class="fa fa-heart pull-left media-object"></i>
                                <div class="media-body">
                                    <h4 class="media-heading">SOCIAL MEDIA</h4>
                                    <p>In suscipit, purus id ultrices sodales, nunc mi porta nibh, bibendum maximus sem mi id metus. Suspendisse faucibus suscipit diam.</p>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInDown" data-wow-delay="2000">
                        <div class="media">
                            <i class="fa fa-laptop pull-left media-object"></i>
                                <div class="media-body">
                                    <h4 class="media-heading">RESPONSIVE THEMES</h4>
                                    <p>In suscipit, purus id ultrices sodales, nunc mi porta nibh, bibendum maximus sem mi id metus. Suspendisse faucibus suscipit diam.</p>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInRight" data-wow-delay="2000">
                        <div class="media">
                            <i class="fa fa-envelope pull-left media-object"></i>
                                <div class="media-body">
                                    <h4 class="media-heading">EMAIL HOSTING</h4>
                                    <p>In suscipit, purus id ultrices sodales, nunc mi porta nibh, bibendum maximus sem mi id metus. Suspendisse faucibus suscipit diam.</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end service -->
        <!-- start newsletter -->
        <section id="newsletter" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="text-center col-md-12 wow fadeInDown" data-wow-delay="2000">
                        <h3>CRYSTAL NEWSLETTER</h3>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <p>Mauris mattis vitae libero eget iaculis. Donec et augue quis quam porttitor consectetur ut nec sem. Integer sagittis viverra quam quis ultricies. Aenean risus nisl, consequat at nunc id, tincidunt tristique mauris.</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="2000">
                        <form role="form">
                            <div class="col-md-1"></div>
                            <div class="col-md-6">
                                <input type="email" placeholder="Enter your Email" class="form-control" required>
                            </div>
                            <div class="col-md-4">
                                <input type="submit" value="SUBSCRIBE NOW" class="form-control">
                            </div>
                            <div class="col-md-1"></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- end newsletter -->
        <!-- start work -->
        <section id="work" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="2000">
                        <h3>CRYSTAL WORK</h3>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <p>Mauris mattis vitae libero eget iaculis. Donec et augue quis quam porttitor consectetur ut nec sem. Integer sagittis viverra quam quis ultricies. Aenean risus nisl, consequat at nunc id, tincidunt tristique mauris.</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="2000">
                        <div class="work_thumb">
                            <img src="<?php echo base_url('assets/images/work-img-1.jpg');?>" class="img-responsive" alt="work">
                                <div class="work_overlay">
                                    <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInDown" data-wow-delay="2000">
                        <div class="work_thumb">
                            <img src="<?php echo base_url('assets/images/work-img-2.jpg');?>" class="img-responsive" alt="work">
                                <div class="work_overlay">
                                    <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInRight" data-wow-delay="2000">
                        <div class="work_thumb">
                            <img src="<?php echo base_url('assets/images/work-img-3.jpg');?>" class="img-responsive" alt="work">
                                <div class="work_overlay">
                                    <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="2000">
                        <div class="work_thumb">
                            <img src="<?php echo base_url('assets/images/work-img-4.jpg');?>" class="img-responsive" alt="work">
                                <div class="work_overlay">
                                    <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="2000">
                        <div class="work_thumb">
                            <img src="<?php echo base_url('assets/images/work-img-5.jpg');?>" class="img-responsive" alt="work">
                                <div class="work_overlay">
                                    <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 wow fadeInRight" data-wow-delay="2000">
                        <div class="work_thumb">
                            <img src="<?php echo base_url('assets/images/work-img-1.jpg');?>" class="img-responsive" alt="work">
                                <div class="work_overlay">
                                    <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end work -->
        <!-- start pricing -->
        <section id="pricing" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="2000">
                        <h3>JKUSDAWA PRICING</h3>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <p> Please tell your friends about <span class="blue">template</span><span class="green">mo</span><span class="orange">.com</span> website. Thank you for supporting us. Donec et augue quis quam porttitor consectetur ut nec sem. Integer sagittis viverra quam quis ultricies. Aenean risus nisl, consequat at nunc id, tincidunt tristique mauris.</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-delay="2000">
                        <div class="plan plan_one">
                            <h4 class="plan_title">STARTER</h4>
                            <ul>
                                <li>$20 per month</li>
                                <li>50 GB SPACE</li>
                                <li>300 GB BANDWIDTH</li>
                                <li>50 Premium Designs</li>
                                <li>1-Year Support</li>
                            </ul>
                            <button class="btn btn-warning">SIGN UP</button>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 wow fadeInDown" data-wow-delay="2000">
                        <div class="plan plan_two">
                            <h4 class="plan_title">BUSINESS</h4>
                            <ul>
                                <li>$50 per month</li>
                                <li>200 GB SPACE</li>
                                <li>1,000 GB BANDWIDTH</li>
                                <li>100 Premium Designs</li>
                                <li>2-Year Support</li>
                            </ul>
                            <button class="btn btn-warning">SIGN UP</button>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 wow fadeInDown" data-wow-delay="2000">
                        <div class="plan plan_three">
                            <h4 class="plan_title">PROFESSIONAL</h4>
                            <ul>
                                <li>$75 per month</li>
                                <li>500 GB SPACE</li>
                                <li>3,000 GB BANDWIDTH</li>
                                <li>200 Premium Designs</li>
                                <li>3-Year Support</li>
                            </ul>
                            <button class="btn btn-warning">SIGN UP</button>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 wow fadeInRight" data-wow-delay="2000">
                        <div class="plan plan_four">
                            <h4 class="plan_title">ADVANCED</h4>
                            <ul>
                                <li>$120 per month</li>
                                <li>1,500 GB SPACE</li>
                                <li>8,000 GB BANDWIDTH</li>
                                <li>500 Premium Designs</li>
                                <li>Lifetime Support</li>
                            </ul>
                            <button class="btn btn-warning">SIGN UP</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end pricing -->
        <!-- start contact -->
        <section id="contact" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="2000">
                        <h3>CONTACT CRYSTAL</h3>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <p>Mauris mattis vitae libero eget iaculis. Donec et augue quis quam porttitor consectetur ut nec sem. Integer sagittis viverra quam quis ultricies. Aenean risus nisl, consequat at nunc id, tincidunt tristique mauris.</p>
                    </div>
                    <div class="col-md-2"></div>
                    <form action="#" method="post">
                        <div class="col-md-4 wow fadeInLeft" data-wow-delay="2000">
                            <input type="text" placeholder="Name" class="form-control">
                        </div>
                        <div class="col-md-4 wow fadeInDown" data-wow-delay="2000">
                            <input type="email" placeholder="Email" class="form-control" required>
                        </div>
                        <div class="col-md-4 wow fadeInRight" data-wow-delay="2000">
                            <input type="text" placeholder="Subject" class="form-control">
                        </div>
                        <div class="col-md-12 wow fadeInDown" data-wow-delay="2000">
                            <textarea placeholder="Message" class="form-control"></textarea>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-6 wow fadeInUp" data-wow-delay="2000">
                            <input type="submit" value="SEND MESSAGE" class="form-control">
                        </div>
                        <div class="col-md-3"></div>
                    </form>
                </div>
            </div>
            <div class="google_map">
                <div id="map-canvas"></div>
            </div>
            <footer class="container">
                <div class="col-md-12 wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.4s">
                    <span>
                        Copyright &copy; 2084 Your Company Name 
                    <!--    | Design: <a rel="nofollow" href="http://www.templatemo.com/page/1"><span class="white">templatemo.com</span></a> -->
                    </span>
                    <ul class="social_icon">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                        <li><a href="#" class="fa fa-apple"></a></li>
                        <li><a href="#" class="fa fa-pinterest"></a></li>
                        <li><a href="#" class="fa fa-google"></a></li>
                        <li><a href="#" class="fa fa-wordpress"></a></li>
                    </ul>
                </div>
            </footer>
        </section>
        <!-- end contact -->

        <!-- start javascript -->
        <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.simple-text-rotator.js');?>"></script>
        <script src="<?php echo base_url('assets/js/smoothscroll.js');?>"></script>
        <script src="<?php echo base_url('assets/js/wow.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.flexslider.js');?>"></script>
        <script src="<?php echo base_url('assets/js/templatemo-script.js');?>"></script>
        <!-- end javascript -->
    </body> 
</html>
