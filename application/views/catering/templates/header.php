<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>JKUSDAWA Caterers</title>
    
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="jkusdawa.com : Jkusda welfare society "/>
    <meta name="twitter:description" content="jkusdawa is a welfare society with departments such as the jkusdawa caterers that cook sumptuous vegetarian food"/>


	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="jkusdawa caterers is under jkusdawa and we are here to give the best vegetarian cuisine">
	<meta name="description" content="caterers">

	<link rel="stylesheet" href="<?php echo base_url('assets2/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets2/css/animate.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets2/css/font-awesome.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets2/css/nivo-lightbox.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets2/css/nivo_themes/default/default.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets2/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets2/dt/build/jquery.datetimepicker.min.css');?>">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
</head>
<body>

<!-- preloader section -->
<section class="preloader">
	<div class="sk-spinner sk-spinner-pulse"></div>
</section>

<!-- navigation section -->
<section class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon icon-bar"></span>
				<span class="icon icon-bar"></span>
				<span class="icon icon-bar"></span>
			</button>
			<a href="#" class="navbar-brand"> <img src="<?php echo base_url('assets2/images/caterers.jpg');?>" width="50px" height="40px"></a>
		

		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#home" class="smoothScroll">HOME</a></li>
				<li><a href="#gallery" class="smoothScroll">FOOD GALLERY</a></li>
				<li><a href="#menu" class="smoothScroll">MENU AND PRICES</a></li>
				<!--<li><a href="#team" class="smoothScroll">CHEFS</a></li>-->
				<li><a href="#contact" class="smoothScroll">ORDER NOW</a></li>
			</ul>
		</div>
	</div>
</section>


<!-- home section -->
<section id="home" class="parallax-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h1>CATERING SERVICES</h1>
				<h2>WELCOME TO HOME OF SWEET VEGETARIAN CUISINE</h2>
				<a href="#gallery" class="smoothScroll btn btn-default">Click Here for more</a>
			</div>
		</div>
	</div>		
</section>


<!-- gallery section -->
<section id="gallery" class="parallax-section">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
				<h1 class="heading">Food Gallery</h1>
				<hr>
			</div>
			<div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
				<a href="<?php echo base_url('assets2/images/cake.jpg');?>" data-lightbox-gallery="zenda-gallery"><img src="<?php echo base_url('assets2/images/cake.jpg');?>" alt="gallery img"></a>
				<div>
					<h3>Cakes And Cookies</h3>
					<span>Fruit</span>
				</div>
				<a href="<?php echo base_url('assets2/images/chapati.jpg');?>" data-lightbox-gallery="zenda-gallery"><img src="<?php echo base_url('assets2/images/chapati.jpg');?>" alt="gallery img"></a>
				<div>
					<h3>Chapati</h3>
					<span>Brown</span>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
				<a href="<?php echo base_url('assets2/images/gallery-img3.jpg');?>" data-lightbox-gallery="zenda-gallery"><img src="<?php echo base_url('assets2/images/gallery-img3.jpg');?>" alt="gallery img"></a>
				<div>
					<h3>Githeri</h3>
					<span>Plain / Mixed</span>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.9s">
				<a href="<?php echo base_url('assets2/images/gallery-img4.jpg');?>" data-lightbox-gallery="zenda-gallery"><img src="<?php echo base_url('assets2/images/gallery-img4.jpg');?>" alt="gallery img"></a>
				<div>
					<h3>Sweet Potato</h3>
					<span>With Beans/ Minji/ Peas</span>
				</div>
				<a href="<?php echo base_url('assets2/images/gallery-img5.jpg');?>" data-lightbox-gallery="zenda-gallery"><img src="<?php echo base_url('assets2/images/gallery-img5.jpg');?>" alt="gallery img"></a>
				<div>
					<h3>Rice</h3>
					<span>With Beans/ Minji/ Peas</span>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- menu section -->
<section id="menu" class="parallax-section">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
				<h1 class="heading">Our Menu</h1>
				<hr>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Rice................ <span>kshs 20</span></h4>
				<h5></h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Beans........................... <span>Kshs 20</span></h4>
				<h5></h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Ndengu........................ <span>Kshs 30</span></h4>
				<h5></h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Peas.......................... <span>Kshs 30</span></h4>
				<h5></h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Chapo...................... <span>Kshs 10</span></h4>
				<h5>Brown</h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Salads........................ <span>Kshs 30/50</span></h4>
				<h5>Vegetable / Fruit </h5>
			</div>
			
			<div class="col-md-6 col-sm-6">
				<h4>Cookies and cakes..................... <span>Kshs 20/10/5</span></h4>
				<h5>Fruit / Lemon</h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Mukimo..................... <span>Kshs 40</span></h4>
		    </div>
			<div class="col-md-6 col-sm-6">
				<h4>Sweet Potatoe stew..................... <span>Kshs 30</span></h4>
				
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Irish Potatoe stew..................... <span>Kshs 20</span></h4>
				
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>French Beans..................... <span>Kshs 30</span></h4>
				
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Uji ................... <span>Kshs 15/10</span></h4>
				<h5>Kubwa / Ndogo</h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Juice..................... <span>Kshs 30</span></h4>
				
			</div>
		</div>
<div class="row">
			<div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
				<h1 class="heading">Packages</h1>
				<hr>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Rice+Minji/Ndengu/Beans+Vegatable... <span>kshs 60</span></h4>
				<h5></h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Chapo(2) + minji/Ndengu/Beans+Vegatable/avocado...<span>Kshs 60</span></h4>
				<h5></h5>
			</div>
			<div class="col-md-6 col-sm-6">
				<h4>Githeri+French beans+Avocado... <span>Kshs 60</span></h4>
				<h5></h5>
			</div>
			
			
		</div>

	</div>
</section>		


<!-- contact section -->
<section id="contact" class="parallax-section">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-1 col-md-10 col-sm-12 text-center">
				<h1 class="heading">ORDER NOW</h1>
				<hr>
			</div>
			<div class="col-md-offset-1 col-md-10 col-sm-12 wow fadeIn" data-wow-delay="0.9s">
				<form id="myform" action="#">
             	<div class="col-md-6 col-sm-6">
						<input name="name" type="text" class="form-control" id="name" placeholder="Name">
				  </div>
					<div class="col-md-6 col-sm-6">
						<input name="phone" type="text" class="form-control" id="phone" placeholder="Phone">
				  </div>
				  <div class="col-md-6 col-sm-6">
						<input name="email" type="email" class="form-control" id="email" placeholder="Email">
				  </div>
				    <div class="col-md-6 col-sm-6">
						<input name="location" type="text" class="form-control" id="location" placeholder="Your Location">
				  </div>
					<div class="col-md-12 col-sm-12">
						<textarea name="order" rows="2" class="form-control" id="order" placeholder="Describe your order here"></textarea>
					</div>
					<div class="col-md-12 col-sm-12">
						<textarea name="direction" rows="4" class="form-control" id="direction" placeholder="Directions if you need delivery"></textarea>
					</div>
					<div class="col-md-6 col-sm-6">
						<input name="time" type="datetime" class="form-control" id="time" placeholder="Time of Delivery/Picking" >
				  </div>
					<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
						<input name="submit" type="submit" class="form-control" id="submit" value="Submit" >
					</div>
				</form>
			</div>
			<div class="col-md-2 col-sm-1"></div>
		</div>
	</div>
</section>


<!-- footer section -->
<footer class="parallax-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
				<h2 class="heading">Contact Info.</h2>
				<div class="ph">
					<p><i class="fa fa-phone"></i> Phone</p>
					<h4>0707294303</h4>
				</div>
				<div class="address">
					<p><i class="fa fa-map-marker"></i> Our Location</p>
					<h4>JUJA</h4>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
				<h2 class="heading">Open Hours</h2>
				   <p>Monday-Thursday <span>8:30 AM - 9:00 PM</span></p>
				   <p>Friday <span>8:30 AM - 5:00 PM</span></p>
				   <p>Saturday <span>CLOSED</span></p>
					<p>Sunday <span>8:30 AM - 9:00 PM</span></p>
		</div>
	</div>
</footer>


<!-- copyright section -->


<!-- JAVASCRIPT JS FILES -->	
<script src="<?php echo base_url('assets2/js/jquery.js');?>"></script>
<script src="<?php echo base_url('assets2/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets2/js/jquery.parallax.js');?>"></script>
<script src="<?php echo base_url('assets2/js/smoothscroll.js');?>"></script>
<script src="<?php echo base_url('assets2/js/nivo-lightbox.min.js');?>"></script>
<script src="<?php echo base_url('assets2/js/wow.min.js');?>"></script>
<script src="<?php echo base_url('assets2/js/custom.js');?>"></script>
<script src="<?php echo base_url('assets2/dt/build/jquery.datetimepicker.full.min.js');?>"></script>

</body>
</html>