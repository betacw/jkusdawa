        <!-- Contact -->
           <div class="show-menu">
            <a href="#" class="shadow-top-down">+</a>
        </div>
        <nav class="main-menu shadow-top-down">
            <ul class="nav nav-pills nav-stacked">
                <li><a class="scroll_effect" href="<?php echo site_url('superclass/index')?>">Home</a></li>
                <li><a class="scroll_effect" href="#templatemo_element">Members</a></li>
            </ul>
        </nav>
        <section id="templatemo_contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <header class="template_header">
                            <h1 class="text-center"><span>...</span> Member Login <span>...</span></h1>
                        </header>
                        <p class="text-center">
                            <i class="fa fa-map-marker"></i> Jomo Kenyatta University<br />
                            <i class="fa fa-envelope"></i> Email: <a href="mailto:info@company.com">info@jkusdawa.co.ke</a><br />
                            <i class="fa fa-phone"></i> Phone: <a href="tel:010-020-0340">+254737367642</a>
                        </p>
                    </div>
                </div>
                <?php echo form_open('superclass/login_feedback');?>
                <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                            <input type="text" name="Email" class="form-control" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <input type="pasword" name="kiingilio" class="form-control" id="contact-name" placeholder="Password" required>
                        </div>
                    </div>
                    
                  
                    <div class="form-group row">
                        <div class="col-xs-6 col-xs-offset-6">
                            <button type="submit" class="form-control">Login</button>
                        </div>
                      
                    </div>
                    Are you a member?<a href='<?php echo site_url('superclass/registration');?>'>Register</a>

                    
                </form>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <ul class="nav nav-pills social">
                            <li><a href="#" class="shadow-top-down social-facebook"><i class="fa fa-facebook-official"></i></a></li>
                            <li><a href="#" class="shadow-top-down social-twitter"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a href="#" class="shadow-top-down social-youtube"><i class="fa fa-youtube-square"></i></a></li>
                            <li><a href="#" class="shadow-top-down social-instagram"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>