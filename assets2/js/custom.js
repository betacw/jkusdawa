
// preloader
$(window).load(function(){
    $('.preloader').fadeOut(1000); // set duration in brackets    
});

/* HTML document is loaded. DOM is ready. 
-------------------------------------------*/
$(function(){

  // ------- WOW ANIMATED ------ //
  wow = new WOW(
  {
    mobile: false
  });
  wow.init();

  // ------- JQUERY PARALLAX ---- //
  function initParallax() {
    $('#home').parallax("100%", 0.1);
    $('#gallery').parallax("100%", 0.3);
    $('#menu').parallax("100%", 0.2);
    $('#team').parallax("100%", 0.3);
    $('#contact').parallax("100%", 0.1);

  }
  initParallax();

  // HIDE MOBILE MENU AFTER CLIKING ON A LINK
  $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });

  // NIVO LIGHTBOX
  $('#gallery a').nivoLightbox({
        effect: 'fadeScale',
    });

});
   $(function() {
    // jQuery('#datetimepicker').datetimepicker();
    jQuery( "#date_11" ).datetimepicker({
  format:'d.m.Y H:i',
  minDate: Date.now(),
  inline:false,
  lang:'en'
    }



      );  
   // $( "#date_22" ).datepicker({ changeMonth: true,   changeYear: true, dateFormat:'yy-mm-dd' }); 
  //  $(function() {
  //   $( "#tabs" ).tabs();
  // });
  
  });
// function letsgo(){
// console.log($('#myform').serialize());


// }

$( "#myform" ).submit(function( event ) {
  event.preventDefault();
$.post( "superclass/send",$( "#myform" ).serialize(), function( data ) {
  data=JSON.parse(data);
if(data.status){
  console.log(data.message);
}else{
datea=data.message.split("\n");
  for (mine in datea){
    if(datea[mine].includes("email")){
      // console.log(datea[mine]);
      $('#email').val(datea[mine].replace(/\<p>/g,'').replace('</p>',''));
    }else if(datea[mine].includes("phone")){
$('#phone').val(datea[mine].replace(/\<p>/g,'').replace('</p>',''));
    }else if(datea[mine].includes("order")){
$('#order').val(datea[mine].replace(/\<p>/g,'').replace('</p>',''));
    }else if(datea[mine].includes("name")){
$('#name').val(datea[mine].replace(/\<p>/g,'').replace('</p>',''));
    }
  }
}
  
});
});

 
  